const functions = require('firebase-functions')
const cors = require('cors')({ origin: true })
const admin = require('firebase-admin')
admin.initializeApp()

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions

const verificarToken = (request, response, next) => {
  try {
    let token = request.headers.authorization
    if (token !== undefined && token !== null && token !== '') {
      token = token.substring(7)
      admin
        .auth()
        .verifyIdToken(token)
        .then(res => {
          request.query.uidCurrentUser = res.uid
          return next()
        })
        .catch(err => {
          response.json(err)
        })
    } else {
      response.status(401).json('not autorhorized')
    }
  } catch (err) {
    response.status(401).json('not autorhorized')
  }
}

const verificarUserAdmin = (request, response, next) => {
  try {
    admin
      .auth()
      .getUser(request.query.uidCurrentUser)
      .then(user => {
        if (user.customClaims.admin) {
          return next()
        } else {
          return response.status(403).send('forbidden')
        }
      })
      .catch(err => response.status(403).json(err))
  } catch (err) {
    response.status(403).json('forbidden')
  }
}

exports.teste = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    verificarToken(request, response, () => {
      verificarUserAdmin(request, response, () => {
        response.send('teste')
      })
    })
  })
})

exports.createUser = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    verificarToken(request, response, () => {
      verificarUserAdmin(request, response, () => {
        return admin
          .auth()
          .createUser({
            email: request.body.email,
            password: request.body.password
          })
          .then(res => {
            admin.auth().setCustomUserClaims(res.uid, {
              admin: request.body.master === '1'
            })
            response.json(res)
            return true
          })
          .catch(err => response.status(400).send(err))
      })
    })
  })
})

exports.updateUser = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    verificarToken(request, response, () => {
      verificarUserAdmin(request, response, () => {
        let data = { email: request.body.email }
        let pass = request.body.password
        if (pass !== undefined && pass !== null && pass !== '') {
          data.password = pass
        }

        return admin
          .auth()
          .updateUser(request.query.uid, data)
          .then(res => {
            admin.auth().setCustomUserClaims(res.uid, {
              admin: request.body.master === '1'
            })
            response.json(res)
            return 0
          })
          .catch(err => response.status(500).send(err))
      })
    })
  })
})

exports.deleteUser = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    verificarToken(request, response, () => {
      verificarUserAdmin(request, response, () => {
        return admin
          .auth()
          .deleteUser(request.query.uid)
          .then(res => response.json(res))
          .catch(err => response.status(500).send(err))
      })
    })
  })
})

exports.indexUser = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    verificarToken(request, response, () => {
      verificarUserAdmin(request, response, () => {
        return admin
          .auth()
          .listUsers()
          .then(res => response.json(res))
          .catch(err => response.status(500).send(err))
      })
    })
  })
})

exports.getUser = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    verificarToken(request, response, () => {
      verificarUserAdmin(request, response, () => {
        return admin
          .auth()
          .getUser(request.query.uid)
          .then(res => response.json(res))
          .catch(erro => response.status(500).send(erro))
      })
    })
  })
})

exports.setMaster = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    verificarToken(request, response, () => {
      verificarUserAdmin(request, response, () => {
        admin
          .auth()
          .setCustomUserClaims(request.query.uid, {
            admin: request.body.master === '1'
          })
          .then(() => response.send('ok'))
          .catch(erro => response.status(500).send(erro))
      })
    })
  })
})

exports.isMaster = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    request.query.uidCurrentUser = request.query.uid
    admin
      .auth()
      .getUser(request.query.uid)
      .then(user => response.send(user.customClaims.admin))
      .catch(erro => response.status(500).send(erro))
  })
})

exports.BWxOTVjZPO0he54uNny = functions.https.onRequest((request, response) => {
  admin
    .auth()
    .setCustomUserClaims(request.query.uid, {
      admin: true
    })
    .then(() => response.send('ok'))
    .catch(erro => response.status(500).send(erro))
})
