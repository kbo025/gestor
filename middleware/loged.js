export default function({ store, redirect, app, route }) {
  if (store.state.authUser === null) {
    const user = app.$cookies.get('authuser')
    const master = app.$cookies.get('master')
    if (typeof user === 'object' && user !== null) {
      store.dispatch('login', { user })
      store.dispatch('master', { master })
      if (route.path === '/login') {
        redirect('/')
      }
    }
  }
}
