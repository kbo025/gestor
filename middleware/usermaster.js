export default function({ store, redirect }) {
  if (!store.state.master) {
    redirect('/')
  }
}
