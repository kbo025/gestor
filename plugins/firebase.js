import * as firebase from 'firebase/app'
import 'firebase/storage'
import 'firebase/auth'
import 'firebase/database'

// const saltRounds = 10
const config = {
  apiKey: 'AIzaSyBcbjJ9zTiOeBv_UMmUOgRkPRRC9I-CHD4',
  authDomain: 'gestor-37704.firebaseapp.com',
  databaseURL: 'https://gestor-37704.firebaseio.com',
  projectId: 'gestor-37704',
  storageBucket: 'gestor-37704.appspot.com',
  messagingSenderId: '771933507904'
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

const db = firebase.database()
const auth = firebase.auth()

/**
 * Obtenemos el usuario actualmente logado
 */
const currentUser = () => {
  return auth.currentUser
}

/**
 * Hacemos un login contra firebase
 */
const login = async (email, password) => {
  const loginStatus = await auth
    .signInWithEmailAndPassword(email, password)
    .catch(err => {
      return err
    })
  return loginStatus
}

/**
 * Hacemos un logout contra firebase
 */
const logout = async () => {
  const logoutStatus = await auth.signOut().catch(err => {
    return err
  })

  return logoutStatus
}

/**
 * Función para hacer consultas a una coleccion
 */
const prospeccao = async (limit = 10) => {
  const snapshot = await db
    .ref('prospeccao')
    .orderByKey()
    .limitToLast(limit)
    .once('value')

  return snapshot.val()
}

/**
 * Función para hacer consultas a una coleccion
 */
const prospeccaoByDate = async (data = []) => {
  let result = []

  for (let i = 0; i < data.length; i++) {
    let current = await db
      .ref('prospeccao')
      .orderByChild('dt')
      .equalTo(data[i])
      .once('value')

    current = current.val()
    if (current != null) {
      current = Object.values(current)
      if (Array.isArray(current)) {
        result = result.concat(current)
      }
    }
  }

  return result
}

export { prospeccao, login, currentUser, logout, prospeccaoByDate }
