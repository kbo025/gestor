export const strict = false

export const state = () => ({
  authUser: null,
  master: false
})

export const mutations = {
  SET_USER: function(state, user) {
    state.authUser = user
  },
  SET_IS_MASTER: function(state, isMaster) {
    state.master = isMaster
  }
}

export const actions = {
  // Definimos la acción login a la cual llamaremos desde nuestra página de login
  login({ commit }, { user }) {
    commit('SET_USER', user)
  },

  // Definos la acción logout a la cual se le va llamar desde nuestro boton de logout
  logout({ commit }) {
    commit('SET_USER', null)
  },

  // Definos la acción logout a la cual se le va llamar desde nuestro boton de logout
  master({ commit }, admin) {
    commit('SET_IS_MASTER', admin)
  }
}
